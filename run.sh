python3 apply_selection.py  --input-file ../raw_data/11md.root \
                                        ../raw_data/11mu.root \
                                        ../raw_data/12md.root \
                                        ../raw_data/12mu.root \
                                        ../raw_data/15md.root \
                                        ../raw_data/15mu.root \
                                        ../raw_data/16md.root \
                                        ../raw_data/16mu.root \
                                        ../raw_data/17md.root \
                                        ../raw_data/17mu.root \
                                        ../raw_data/18md.root \
                                        ../raw_data/18mu.root \
                            --output-file selected.root \
                            --mode B2PsiKK  \
                            --selection-files cuts.yaml \
                            --branches-files branches.yaml \
                            --year 2011 \
                            --input-tree-name B2Jpsif0Tree/mytree