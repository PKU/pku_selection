double calculatePlaneAngle(TLorentzVector pMp,
    TLorentzVector pMm,
    TLorentzVector pKp,
    TLorentzVector pKm)
{
    TLorentzVector pJpsi = pMp + pMm;
    TLorentzVector pPhi = pKp + pKm;
    TLorentzVector pB = pJpsi + pPhi;
    TVector3 boosttoparent = -pB.BoostVector();
    pMp.Boost(boosttoparent);
    pMm.Boost(boosttoparent);
    pKp.Boost(boosttoparent);
    pKm.Boost(boosttoparent);
    pJpsi.Boost(boosttoparent);
    pPhi.Boost(boosttoparent);

    TVector3 vecA = pMp.Vect().Unit();
    TVector3 vecB = pMm.Vect().Unit();
    TVector3 vecC = pKp.Vect().Unit();
    TVector3 vecD = pKm.Vect().Unit();

    TVector3 el = (vecA.Cross(vecB)).Unit();
    TVector3 ek = (vecC.Cross(vecD)).Unit();
    TVector3 ez = (pJpsi.Vect()).Unit();

    double cosPhi = (ek.Dot(el));
    double sinPhi = (el.Cross(ek)).Dot(ez);
    double phi = acos(cosPhi);

    // Resolve ambiguity
    return (sinPhi > 0.0 ? phi : -phi);
}