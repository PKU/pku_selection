This is a selection framework of PKU group.

The main script is the ```apply_selection.py``` file.
You do not need to understand everything in the file.

```run.sh``` is an example of how to config the parameters.

The branches you want to keep in your selected root files is placed in a yaml file named ```branches.yaml```.
In the yaml file, key is the branch name in output branch, and the value is how to calculate the branch.
The meaning of key and value in the yaml file is shown below:
```
key: value
```

The cuts you want to apply stored in ```cuts.yaml```.
The key is the name of the cut, the value is the cuts which you want to apply.

The most complicated case is calculating helicity angles, which is achieved by a ```cpp``` file, named as ```calculatePlaneAngle.cpp```.
You need to add a line in ```apply_selection.py``` file to make it avaliable.
```
gInterpreter.LoadMacro('calculatePlaneAngle.cpp')
```
You can find how to use the function ```calculatePlaneAngle``` in the ```branches.yaml``` file, shown below:
```
"chi": "calculatePlaneAngle(vec4_mup_PVFitBs, vec4_mum_PVFitBs, vec4_pip_PVFitBs, vec4_pim_PVFitBs)"
```


If you want to add additional branches and keep all the original branches, just set the python flag:
```
--keep-all-original-branches True
``` 

NOTE:
1. You cannot define a output branch with the same name twice.
   The second time definition does not work.

Any question, ask Zhihong Shen.
